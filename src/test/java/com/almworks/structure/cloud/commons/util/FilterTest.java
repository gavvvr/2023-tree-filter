package com.almworks.structure.cloud.commons.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.function.IntPredicate;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Named.named;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class FilterTest {
    @Test
    public void testFilter() {
        Hierarchy unfiltered = new ArrayBasedHierarchy(
                //        x  x     x  x     x  x     x   x
                new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11},
                new int[]{0, 1, 2, 3, 1, 0, 1, 0, 1, 1, 2}
        );
        Hierarchy filteredActual = Filter.filter(unfiltered, nodeId -> nodeId % 3 != 0);
        Hierarchy filteredExpected = new ArrayBasedHierarchy(
                new int[]{1, 2, 5, 8, 10, 11},
                new int[]{0, 1, 1, 0, 1, 2}
        );

        assertEquals(filteredExpected.formatString(), filteredActual.formatString());
    }

    @DisplayName("Hierarchy filter test")
    @ParameterizedTest(name = "{index}: ''{0} on input'' and ''{1}''")
    @MethodSource("testArguments")
    public void parameterizedFilterTest(Hierarchy input, IntPredicate nodeIdPredicate, Hierarchy filteredExpected) {
        Hierarchy filteredActual = Filter.filter(input, nodeIdPredicate);
        assertEquals(filteredExpected.formatString(), filteredActual.formatString());
    }

    static Stream<Arguments> testArguments() {
        return Stream.of(
                arguments(
                        named("empty hierarchy", new ArrayBasedHierarchy(
                                new int[]{},
                                new int[]{}
                        )),
                        named("whatever filter", (IntPredicate) nodeId -> false),
                        new ArrayBasedHierarchy(
                                new int[]{},
                                new int[]{}
                        )
                ),
                arguments(
                        named("single root node", new ArrayBasedHierarchy(
                                new int[]{1},
                                new int[]{0}
                        )),
                        named("no filter", (IntPredicate) nodeId -> true),
                        new ArrayBasedHierarchy(
                                new int[]{1},
                                new int[]{0}
                        )
                ),
                arguments(
                        named("single root node", new ArrayBasedHierarchy(
                                new int[]{1},
                                new int[]{0}
                        )),
                        named("rejecting everything filter", (IntPredicate) nodeId -> false),
                        new ArrayBasedHierarchy(
                                new int[]{},
                                new int[]{}
                        )
                ),
                arguments(
                        named("2 single-node trees", new ArrayBasedHierarchy(
                                new int[]{1, 2},
                                new int[]{0, 0}
                        )),
                        named("filter matching 1st tree", (IntPredicate) nodeId -> nodeId == 1),
                        new ArrayBasedHierarchy(
                                new int[]{1},
                                new int[]{0}
                        )
                ),
                arguments(
                        named("two single-node trees", new ArrayBasedHierarchy(
                                new int[]{1, 2},
                                new int[]{0, 0}
                        )),
                        named("filter matching 2nd tree", (IntPredicate) nodeId -> nodeId == 2),
                        new ArrayBasedHierarchy(
                                new int[]{2},
                                new int[]{0}
                        )
                ),
                arguments(
                        named("two single-node trees", new ArrayBasedHierarchy(
                                new int[]{1, 2},
                                new int[]{0, 0}
                        )),
                        named("no filtering", (IntPredicate) nodeId -> true),
                        new ArrayBasedHierarchy(
                                new int[]{1, 2},
                                new int[]{0, 0}
                        )
                ),
                arguments(
                        named("single tree of 2 nodes", new ArrayBasedHierarchy(
                                new int[]{1, 2},
                                new int[]{0, 1}
                        )),
                        named("no filtering", (IntPredicate) nodeId -> true),
                        new ArrayBasedHierarchy(
                                new int[]{1, 2},
                                new int[]{0, 1}
                        )
                ),
                arguments(
                        named("single tree of 2 nodes", new ArrayBasedHierarchy(
                                new int[]{1, 2},
                                new int[]{0, 1}
                        )),
                        named("filter excluding child node", (IntPredicate) nodeId -> nodeId != 2),
                        new ArrayBasedHierarchy(
                                new int[]{1},
                                new int[]{0}
                        )
                ),
                arguments(
                        named("single tree of 2 nodes", new ArrayBasedHierarchy(
                                new int[]{1, 2},
                                new int[]{0, 1}
                        )),
                        named("filter excluding root node", (IntPredicate) nodeId -> nodeId != 1),
                        new ArrayBasedHierarchy(
                                new int[]{},
                                new int[]{}
                        )
                ),
                arguments(
                        named("balanced tree of 3 nodes", new ArrayBasedHierarchy(
                                new int[]{1, 2, 3},
                                new int[]{0, 1, 1}
                        )),
                        named("filter excluding 1st child node", (IntPredicate) nodeId -> nodeId != 2),
                        new ArrayBasedHierarchy(
                                new int[]{1, 3},
                                new int[]{0, 1}
                        )
                ),
                arguments(
                        named("balanced tree of 3 nodes", new ArrayBasedHierarchy(
                                new int[]{1, 2, 3},
                                new int[]{0, 1, 1}
                        )),
                        named("filter excluding 2nd child node", (IntPredicate) nodeId -> nodeId != 3),
                        new ArrayBasedHierarchy(
                                new int[]{1, 2},
                                new int[]{0, 1}
                        )
                ),
                arguments(
                        named("balanced tree of 3 nodes", new ArrayBasedHierarchy(
                                new int[]{1, 2, 3},
                                new int[]{0, 1, 1}
                        )),
                        named("filter excluding root node", (IntPredicate) nodeId -> nodeId != 1),
                        new ArrayBasedHierarchy(
                                new int[]{},
                                new int[]{}
                        )
                ),
                arguments(
                        named("tree with decreasing depth on DFS traversal", new ArrayBasedHierarchy(
                                new int[]{1, 2, 3, 4},
                                new int[]{0, 1, 2, 1}
                        )),
                        named("filter excluding 1st child node", (IntPredicate) nodeId -> nodeId != 2),
                        new ArrayBasedHierarchy(
                                new int[]{1, 4},
                                new int[]{0, 1}
                        )
                ),
                arguments(
                        named("tree with decreasing depth on DFS traversal", new ArrayBasedHierarchy(
                                new int[]{1, 2, 3, 4},
                                new int[]{0, 1, 2, 1}
                        )),
                        named("filter excluding tail of 1st child node", (IntPredicate) nodeId -> nodeId != 3),
                        new ArrayBasedHierarchy(
                                new int[]{1, 2, 4},
                                new int[]{0, 1, 1}
                        )
                ),
                arguments(
                        named("tree with decreasing depth on DFS traversal", new ArrayBasedHierarchy(
                                new int[]{1, 2, 3, 4},
                                new int[]{0, 1, 2, 1}
                        )),
                        named("filter excluding 2nd child node", (IntPredicate) nodeId -> nodeId != 4),
                        new ArrayBasedHierarchy(
                                new int[]{1, 2, 3},
                                new int[]{0, 1, 2}
                        )
                ),
                arguments(
                        named("tree with decreasing depth on DFS traversal", new ArrayBasedHierarchy(
                                new int[]{1, 2, 3, 4},
                                new int[]{0, 1, 2, 1}
                        )),
                        named("no filtering", (IntPredicate) nodeId -> true),
                        new ArrayBasedHierarchy(
                                new int[]{1, 2, 3, 4},
                                new int[]{0, 1, 2, 1}
                        )
                ),
                arguments(
                        named("two trees", new ArrayBasedHierarchy(
                                new int[]{1, 2, 3, 4},
                                new int[]{0, 1, 0, 1}
                        )),
                        named("filter excluding root of the 2nd tree", (IntPredicate) nodeId -> nodeId != 3),
                        new ArrayBasedHierarchy(
                                new int[]{1, 2},
                                new int[]{0, 1}
                        )
                )
        );
    }
}